package com.gsk.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.gsk.common.serivce.LDAPService;
import com.gsk.rosettaStone.entity.RosettaStoneResponseEntity;
import com.gsk.rosettaStone.handler.MongoHandler;
import com.gsk.rosettaStone.handler.ValidationHandler;
import com.gsk.rosettaStone.service.UserService;

import io.jsonwebtoken.lang.Collections;

@RestController
public class RosettaController {

	@Autowired
	private MongoHandler mongoHandler;

	@Autowired
	private UserService userService;

	@Autowired
	private ValidationHandler validationHandler;

	@Autowired
	LDAPService ldapService;

	@GetMapping("/api/**")
	public ResponseEntity<RosettaStoneResponseEntity> rosettaFrontController(HttpServletRequest request)
			throws Exception {

		// <server>:<port>/api/<version>/<JSON-DOC-NAME>/<Env>/<Cluster>/<Tool>/xxx

		String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

		AntPathMatcher apm = new AntPathMatcher();
		String finalPath = apm.extractPathWithinPattern(bestMatchPattern, path);

		String[] config = finalPath.split("/");

		String mudId = request.getParameter("mudId");
		String sourceSystem = request.getParameter("sourceSystem");

		System.out.println("User Id is " + mudId);

		if (StringUtils.isEmpty(sourceSystem)) {
			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			response.setUserAccess(null);
			response.setJson(null);
			response.setMessage("sourceSystem should not be empty");
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}

		if (StringUtils.isEmpty(mudId)) {
			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			response.setUserAccess(null);
			response.setJson(null);
			response.setMessage("mudId should not be empty");
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}

		boolean validUser = false;
		try {
			Map<String, Object> userAttributes = ldapService.search(mudId);
			List<Map<String, Object>> listOfMap = ldapService.searchByField("cn", mudId);
			validUser = !Collections.isEmpty(listOfMap);

		} catch (Exception e) {
			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			response.setUserAccess(null);
			response.setJson(null);
			response.setMessage("User not found");
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
		if (!validUser) {
			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			response.setUserAccess(null);
			response.setJson(null);
			response.setMessage("Invalid User");
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}

		boolean isSourceSystemValid = validationHandler.validateSourceSystem(sourceSystem);

		if (!isSourceSystemValid) {

			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			response.setUserAccess(null);
			response.setJson(null);
			response.setMessage("sourceSystem is not valid");
			return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);

		}

		RosettaStoneResponseEntity response = mongoHandler.inMemoryHandler(config, mudId);
		response.setMessage(null);
		return new ResponseEntity(response, HttpStatus.OK);

	}

	@GetMapping("/userAccessCheck/{mudId}/{groupName}")
	public ResponseEntity<String> mongoHit(@PathVariable("mudId") String mudId,
			@PathVariable("groupName") String groupName) throws IOException {

		// mongoHandler.execute(input);
		userService.checkAccess(mudId, groupName);
		return new ResponseEntity("Hello World! ", HttpStatus.OK);
	}

	@GetMapping("/mongoHit/{input}")
	public ResponseEntity<String> mongoHitter(@PathVariable("input") String input) {

		mongoHandler.execute(input);
		return new ResponseEntity("Hello World! ", HttpStatus.OK);
	}
}
