package com.gsk.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InfoController {
	@GetMapping("/info")
	public void info(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException {
		httpServletResponse.sendRedirect("/swagger-ui.html");
	}

	@GetMapping("/health")
	public ResponseEntity health(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException {
		return new ResponseEntity(HttpStatus.OK);
	}
}
