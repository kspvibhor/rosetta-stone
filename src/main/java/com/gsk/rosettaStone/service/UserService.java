package com.gsk.rosettaStone.service;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Component
public class UserService {

	public static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");
	@Value("${gsk-hostname}")
	private String gsk_hostname;

	@Value("${user-ws.port}")
	private String gsk_user_ws_port;

	public String checkAccess(String userId, String groupId) throws IOException {
		String url = buildUrl(userId, groupId);

		StringBuilder accessResponse = new StringBuilder();

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(url).build();

		Response response = client.newCall(request).execute();

		accessResponse.append(response.body().string());

		return accessResponse.toString();
	}

	private String buildUrl(String mudId, String groupId) {
		StringBuilder str = new StringBuilder();
		str.append("http://");
		str.append(gsk_hostname);

		if (!StringUtils.contains(gsk_hostname, "corpnet2.com")) {
			str.append(".corpnet2.com");
		}
		str.append(":");
		str.append(gsk_user_ws_port);
		str.append("/isUserInGroup?mudId=");
		str.append(mudId);
		str.append("&groupId=");
		str.append(groupId);

		System.out.println("User ws url is : " + str.toString());
		return str.toString();
	}

}
