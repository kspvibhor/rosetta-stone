package com.gsk.rosettaStone.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mongodb.DBObject;

@Document(collection = "rosetta_stone")
public class Config {

	private DBObject json;
	@Id
	private String id;

	public DBObject getJson() {
		return json;
	}

	public void setJson(DBObject json) {
		this.json = json;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Config [json=" + json + ", id=" + id + "]";
	}

}
