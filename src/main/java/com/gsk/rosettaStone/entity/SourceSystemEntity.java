package com.gsk.rosettaStone.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "source_sytem_list")
public class SourceSystemEntity {
	@Id
	String sourceSystem;

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	@Override
	public String toString() {
		return "SourceSystemEntity [sourceSystem=" + sourceSystem + "]";
	}

}
