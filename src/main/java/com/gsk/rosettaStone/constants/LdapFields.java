package com.gsk.rosettaStone.constants;

public enum LdapFields {
	EMAIL("mail"), MANAGER("manager"), GIVENNAME("givenName"), DISTINGUISHED_NAME("distinguishedName"), COMPANY(
			"company"), EXTENSION_ATTRIBUTE("extensionAttribute13"), MEMBEROF("memberOf");
	private String fieldName;

	LdapFields(String fieldName) {
		this.setFieldName(fieldName);
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}
