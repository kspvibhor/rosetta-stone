package com.gsk.rosettaStone.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gsk.rosettaStone.entity.SourceSystemEntity;

public interface SourceSystemRepository extends MongoRepository<SourceSystemEntity, String> {

}
