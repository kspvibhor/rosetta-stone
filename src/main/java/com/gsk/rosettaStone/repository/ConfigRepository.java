package com.gsk.rosettaStone.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gsk.rosettaStone.entity.Config;

@Repository
public interface ConfigRepository extends MongoRepository<Config, String> {

}
