package com.gsk.rosettaStone.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.gsk.rosettaStone.entity.Config;
import com.gsk.rosettaStone.entity.RosettaStoneResponseEntity;
import com.gsk.rosettaStone.entity.SourceSystemEntity;
import com.gsk.rosettaStone.repository.ConfigRepository;
import com.gsk.rosettaStone.repository.SourceSystemRepository;
import com.gsk.rosettaStone.service.UserService;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class MongoHandler {

	@Autowired
	private UserService userService;

	@Autowired
	private ConfigRepository configRepo;

	@Autowired
	private SourceSystemRepository sourceSystemRepo;

	public void execute(String input) {

		try {

			// MongoTemplate mongoTemplate = getMongoTemplate();
			// 1. BasicDBObject example

			Config config = new Config();

			String json2 = "{'rdip-configurations': { 'scc':{ 'dev':{ 'aka':'dev-scc', 'anaconda':"
					+ "{'ldap_admin_group':'dev_rip_adm_grp', 'ldap_all_user_group': 'dev_rip_allusr_grp', 'server':'us1salxhpu0015.corpnet2.com', 'port':443, 'url':'https://us1salxhpu0015.corpnet2.com:443/' }, "
					+ "'hue':{ 'server':'xyz', 'port':1234 }, 'edge_nodes':[ 'us1salxhpe0009.corpnet2.com' ] }, "
					+ "'test':{ 'aka':'test-scc', 'anaconda':{ 'server':'us1salxhpu0015.corpnet2.com', 'port':443, "
					+ "'url':'https://us1salxhpu0015.corpnet2.com:443/' }, 'hue':{ 'server':'xyz', 'port':1234 } },"
					+ " 'prod':{ 'aka':'prod-scc', 'anaconda':{ 'server':'us1salxhpu0015.corpnet2.com', 'port':443, "
					+ "'url':'https://us1salxhpu0015.corpnet2.com:443/' }, 'hue':{ 'server':'xyz', 'port':1234 }, "
					+ "'edge_nodes':[ 'us1salxhpe0009.corpnet2.com' ] } } } }";

			String json3 = "{ 'conf': { 'snd': { 'ca_url': 'http://repository.rdip.gsk.com/hadoop-repo/scripts/cluster_cloudera/certs_gen/security/cacerts.pem', 'dev': { 'aka': 'sandbox', 'cloudera_manager': { 'server': { 'host': [ 'https://us1salxhpu0014.corpnet2.com' ], 'port': [ 7183 ] } }, 'anaconda': { 'ldap_admin_group': 'dev_rip_adm_grp', 'ldap_all_user_group': 'dev_rip_allusr_grp', 'server:': { 'host': [ 'https://us1salxhpu0015.corpnet2.com' ], 'port': [ 443 ] } }, 'hive': { 'server': { 'host' : [ 'us1salxhpu0015.corpnet2.com' ], 'port': [ 10000 ] }, 'vanity_url': null }, 'hue': { 'load_balancer': null, 'server': { 'host': [ 'https://us1salxhpu0014.corpnet2.com' ], 'port': [ 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0009.corpnet2.com' ] }, 'impala': { 'daemon': null, 'port': null, 'state_store': null, 'catalogd': null, 'vanity_url': 'impala-snd.rdip.gsk.com' }, 'rconnect': { 'server': { 'host': [ 'https://us1salxhpe0012.corpnet2.com' ], 'port': [ 3939 ] } }, 'rstudio': { 'server': { 'host': [ 'https://us1salxhpe0009.corpnet2.com' ], 'port': [ 8787 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0010.corpnet2.com:18088' } } }, 'scc': { 'ca_url': 'http://repository.rdip.gsk.com/hadoop-repo/scripts/cluster_cloudera/certs_gen/security/cacerts.pem', 'dev': { 'aka': 'SCC-dev', 'anaconda': { 'ldap_admin_group': 'dev_rip_adm_grp', 'ldap_all_user_group': 'dev_rip_allusr_grp', 'server': { 'host': [ 'https://aen-dev.rdip.gsk.com/' ], 'port': [ 443 ] } }, 'hue': { 'server': { 'host': [ 'https://us1salxhpu0017.corpnet2.com', 'https://us1salxhpu0020.corpnet2.com' ], 'port': [ 8888, 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0010.corpnet2.com', 'us1salxhpe0011.corpnet2.com' ] }, 'impala': { 'vanity_url': 'impala-dev-scc.rdip.gsk.com' }, 'rconnect': { 'server': { 'host': [ 'https://us1salxhpe0012.corpnet2.com' ], 'port': [ 3939 ] } }, 'rstudio': { 'server': { 'host': [ 'https://us1salxhpe0012.corpnet2.com' ], 'port': [ 8787 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0014.corpnet2.com:18088' } }, 'tst': { 'aka': 'SCC-test', 'anaconda': { 'ldap_admin_group': 'tst_rdip_adm_grp', 'ldap_all_user_group': 'tst_rdip_allusr_grp', 'server': { 'host': [ 'https://aen-tst.rdip.gsk.com/' ], 'port': [ 443 ] } }, 'ca_url': 'http://repository.rdip.gsk.com/hadoop-repo/scripts/cluster_cloudera/certs_gen/security/cacerts.pem', 'hue': { 'server': { 'host': [ 'https://us1salxhpu0023.corpnet2.com', 'https://us1salxhpu0026.corpnet2.com' ], 'port': [ 8888, 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0013.corpnet2.com', 'us1salxhpe0014.corpnet2.com' ] }, 'impala': { 'vanity_url': 'impala-tst-scc.rdip.gsk.com' }, 'rconnect': { 'server': { 'host': [ 'https://us1salxhpe0012.corpnet2.com' ], 'port': [ 3939 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0018.corpnet2.com:18088' } }, 'prd': { 'aka': 'SCC-prod', 'anaconda': { 'ldap_admin_group': 'prd_rip_adm_grp', 'ldap_all_user_group': 'prd_rip_allusr_grp', 'server': { 'host': [ 'https://aen-prd.rdip.gsk.com/' ], 'port': [ 443 ] } }, 'hue': { 'server': { 'host': [ 'https://us1salxhpu0001.corpnet2.com', 'https://us1salxhpu0004.corpnet2.com' ], 'port': [ 8888, 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0001.corpnet2.com', 'us1salxhpe0002.corpnet2.com', 'us1salxhpe0003.corpnet2.com', 'us1salxhpe0004.corpnet2.com', 'us1salxhpe0005.corpnet2.com', 'us1salxhpe0006.corpnet2.com', 'us1salxhpe0007.corpnet2.com', 'us1salxhpe0008.corpnet2.com' ] }, 'impala': { 'vanity_url': 'impala-prd-scc.rdip.gsk.com' }, 'rstudio': { 'server': { 'host': [ 'https://us1salxhp008.corpnet2.com' ], 'port': [ 8787 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0003.corpnet2.com:18088' } } }, 'mcc': { 'ca_url': 'http://repository.rdip.gsk.com/hadoop-repo/scripts/cluster_cloudera/certs_gen/security/cacerts.pem', 'prd': { 'aka': 'MCC-prod', 'anaconda': { 'ldap_admin_group': 'prd_rip_adm_grp', 'ldap_all_user_group': 'prd_rip_allusr_grp', 'server': { 'host': [ 'https://aen-prd.rdip.gsk.com/' ], 'port': [ 443 ] } }, 'hue': { 'server': { 'host': [ 'https://us1salxhpu0034.corpnet2.com', 'https://us1salxhpu0037.corpnet2.com' ], 'port': [ 8888, 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0017.corpnet2.com' ] }, 'impala': { 'vanity_url': 'impala-prd-mcc.rdip.gsk.com' }, 'rstudio': { 'server': { 'host': [ 'https://us1salxhp017.corpnet2.com' ], 'port': [ 8787 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0033.corpnet2.com:18088' } }, 'dev': { 'aka': 'MCC-dev', 'anaconda': { 'ldap_admin_group': 'dev_rip_adm_grp', 'ldap_all_user_group': 'dev_rip_allusr_grp', 'server': { 'host': [ 'https://aen-dev.rdip.gsk.com/' ], 'port': [ 443 ] } }, 'hue': { 'server': { 'host': [ 'https://us1salxhpu0030.corpnet2.com', 'https://us1salxhpu0033.corpnet2.com' ], 'port': [ 8888, 8888 ] } }, 'edge_node': { 'host': [ 'us1salxhpe0016.corpnet2.com' ] }, 'impala': { 'vanity_url': 'impala-dev-mcc.rdip.gsk.com' }, 'rstudio': { 'server': { 'host': [ 'https://us1salxhp016.corpnet2.com' ], 'port': [ 8787 ] } }, 'spark': { 'history_server': 'http://us1salxhpm0027.corpnet2.com:18088' } } } }, 'repos': { } }";

			DBObject dbObject = (DBObject) JSON.parse(json3);

			config.setJson(dbObject);

			if (StringUtils.isNotEmpty(input)) {
				// configRepo.save(config);
				SourceSystemEntity ssEntity = new SourceSystemEntity();
				ssEntity.setSourceSystem(input);
				// sourceSystemRepo.save(ssEntity);
				return;
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		}

	}

	public RosettaStoneResponseEntity inMemoryHandler(String[] config, String mudId)
			throws JsonParseException, JsonMappingException, IOException {

		Integer offSet = 1;

		RosettaStoneResponseEntity responseEntity = new RosettaStoneResponseEntity();
		/*
		 * MongoTemplate mongoTemplate = getMongoTemplate(); List<Config>
		 * results = mongoTemplate.findAll(Config.class);
		 */
		JsonNode resultantNode = null;

		List<Config> results = (List<Config>) configRepo.findAll();

		// <server>:<port>/api/
		if (StringUtils.isEmpty(config[0])) {
			RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode node = objectMapper.readValue("{ \"version\" : \"v1\" }", JsonNode.class);
			response.setUserAccess(null);
			response.setJson(node);
			return (response);
		}

		// <server>:<port>/api/<version>
		if (config.length == offSet) {
			return (findAllJsonNames(results));
		}

		Boolean userAccess = null;

		Boolean checkAccessFlag = false;
		if (StringUtils.compareIgnoreCase(config[config.length - 1], "url") == 0) {
			checkAccessFlag = true;
		}

		for (Config item : results) {
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode node = objectMapper.readValue(item.getJson().toString(), JsonNode.class);

			if (Objects.nonNull(node.get(config[offSet]))) {

				for (int i = offSet; i < config.length; i++) {

					// when last parameter is being read
					if (i == config.length - 1 && checkAccessFlag) {
						try {
							if (StringUtils.isEmpty(mudId)) {
								userAccess = null;
							} else {
								userAccess = checkAccess(node, mudId);
							}

							responseEntity.setUserAccess(userAccess.toString());
						} catch (Exception e) {

							if (StringUtils.equals(e.getMessage(), "Ldap group not found")) {
								responseEntity.setUserAccess("Ldap group not found");
							}
						}
					}

					if (java.util.Objects.isNull(node)) {
						resultantNode = null;
						break;
					}
					// System.out.println(" Value of i is " + i);
					if (node instanceof ArrayNode) {
						node = node.get(Integer.parseInt(config[i]));
						resultantNode = node;
					} else {
						node = node.get(config[i]);
						resultantNode = node;
					}
				}
			}
		}

		responseEntity.setJson(resultantNode);
		return responseEntity;

		// TODO:
		// 1. Handle case where the node is now null
		// 2 Expectation is there is only 1 Json with a given name, no duplicate
		// entries should exist for same level of nesting
	}

	private RosettaStoneResponseEntity findAllJsonNames(List<Config> results)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		List<String> names = new ArrayList<>();
		for (Config item : results) {
			JsonNode node = objectMapper.readValue(item.getJson().toString(), JsonNode.class);

			Iterator<String> cat_name_it = node.fieldNames();

			// System.out.println(cat_name_it.next());

			names.add(cat_name_it.next());

		}

		// System.out.println(names.toString());
		RosettaStoneResponseEntity response = new RosettaStoneResponseEntity();
		JsonNode node = objectMapper.convertValue(names, JsonNode.class);
		response.setJson(node);
		return response;

	}

	private Boolean checkAccess(JsonNode node, String mudId) throws Exception {

		JsonNode ldap_admin_group_node = node.get("ldap_admin_group");

		if (ldap_admin_group_node == null) {
			throw new Exception("Ldap group not found");
		}
		String ldap_admin_group = ldap_admin_group_node.asText();

		String ldap_admin_group_access = userService.checkAccess(mudId, ldap_admin_group);

		if (StringUtils.compareIgnoreCase(ldap_admin_group_access, "true") == 0) {
			return true;
		}

		JsonNode ldap_all_user_group_node = node.get("ldap_all_user_group");
		if (ldap_all_user_group_node == null) {
			throw new Exception("Ldap group not found");
		}
		String ldap_all_user_group = ldap_all_user_group_node.asText();

		String ldap_all_user_group_access = userService.checkAccess(mudId, ldap_all_user_group);

		if (StringUtils.compareIgnoreCase(ldap_all_user_group_access, "true") == 0) {
			return true;
		}

		return false;
	}

	public MongoTemplate getMongoTemplate() {

		// mongodb://oay19199:bbessacu4931@us1salxhpu0022.corpnet2.com:27006/rdip_portal?
		// authMechanism=PLAIN&authSource=$external&ssl=true
		MongoClient mongoClient = new MongoClient("us1salxhpu0022.corpnet2.com", 27006);
		UserCredentials userCredentials = new UserCredentials("oay19199", "bbessacu4931");
		MongoDbFactory dbFactory = new SimpleMongoDbFactory(mongoClient, "rdip_portal", userCredentials);
		return new MongoTemplate(dbFactory);
	}

}
