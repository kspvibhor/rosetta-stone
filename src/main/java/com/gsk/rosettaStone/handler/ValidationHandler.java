package com.gsk.rosettaStone.handler;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gsk.rosettaStone.entity.SourceSystemEntity;
import com.gsk.rosettaStone.repository.SourceSystemRepository;

@Component
public class ValidationHandler {

	@Autowired
	SourceSystemRepository sourceSystemRepo;

	public boolean validateSourceSystem(String sourceSystem) {

		SourceSystemEntity ssEntity = sourceSystemRepo.findOne(sourceSystem);

		if (Objects.isNull(ssEntity)) {
			return false;
		}

		return true;
	}

}
